# Hello Gitee 项目

无需编程基础，传承至计算机编程中历史悠久的传统项目 “Hello World”，以此为基础设计了Gitee专属的“Hello Gitee”教程

你将学会以下内容:

1. 创建并使用Gitee仓库
2. 什么是分支及分支的作用
3. 创建新的分支
4. 新分支与主分支之间如何协作

## 什么是 Gitee

Gitee 是一个版本控制和协作的代码托管平台(不仅可以托管代码，还可以托管文档与图片资料）。 它可以让你和其他人一起在远程或本地项目上进行协作。

本教程将为您介绍 Gitee 的一些基础知识，如：仓库、分支、分支修改确认以及 Pull Request (代码评审）。您将创建自己的 Hello Gitee 仓库，并学习 Gitee 的 Pull Request 工作流

提示：

1. 您可在单独的浏览器窗口打开此页面，以便按步骤操作时进行查看
2. 在创建仓库前确认自己是否绑定了邮箱，若没有可点击右上角头像，进入设置，在“多邮箱配置”中进行设置

## **五步 Gitee 轻松入门**

### **第一步 创建 Hello Gitee 仓库**

仓库可用于存放自己的一个创意或项目的资料库。

仓库内可包含：文件夹和文件，图片，视频，电子文档和代码等你能想到的任何元素。 

创建新仓库时建议设置一个 README.md 的文档来介绍您的项目。

 （新手默认不选）另外可选开源许可证文件，让你明确声明自己资源他人可以如何使用。

提示：社区版Gitee提供5G的总仓库空间，单文件最大支持50M

详细内容可查看：[https://gitee.com/help/articles/4167](https://gitee.com/help/articles/4167)

#### >仓库创建步骤

1. 创建名字为 hello-gitee 的仓库
2. 仓库介绍可自定义，或直接填写：入门学习
3. 是否开源选择：公开
4. 勾选使用 Readme 文件初始化这个仓库
5. 点击创建按钮

（其他选项使用默认设置即可）

![新建 Gitee 仓库](https://images.gitee.com/uploads/images/2020/0521/153756_d2717d99_2301581.png "新建仓库开源.png")

此时，新建的 hello-gitee 仓库中会有 README.en.md 及 README.md 两个默认 Markdown 格式的文档。我们将在 README.md 文档上进行后续操作。

---

---

### **第二步 创建一个新分支**

分支是给您提供后悔药的一种方式，以自己写的一段文字记录为例，复制出一个一模一样的文档，在新复制出的文档上修改内容，然后再合并到原始文档时，对比两个文档可以看到哪些地方做了修改，如果觉得副本中一些有价值的原始记录内容被删除了，可以继续修改副本，以免一份文档修改后，原本写的有价值的文字也被删除，就得不偿失了。

![小白分支工作流讲解彩色版](C:\Users\acer\Documents\开源中国\项目设计\Gitee入门\小白分支工作流讲解彩色版.png)

#### >创建新分支步骤

1. 点击下图中的“1个分支”进入分支管理界面

![进入 Gitee 分支管理界面](https://images.gitee.com/uploads/images/2020/0521/153942_5a01bc57_2301581.png "新建分支.png")

---

2. 点击右上角“新建分支”按钮，命名“新分支名称”为：caogao，点击“提交”按钮确认

![新建 Gitee 仓库的分支](https://images.gitee.com/uploads/images/2020/0521/154002_16d6c8cf_2301581.png "创建分支并命名.png")

---

3. 点击新建好的分支名“caogao”，即可进入新分支界面。也可如下图红框所示进行切换：

![Gitee 切换分支按钮](https://images.gitee.com/uploads/images/2020/0521/154051_1c21a592_2301581.png "切换分支按钮.png")

到这里，一个新的分支就创建好啦！

---

---

### **第三步 修改分支并提交更改**

让我们直奔主题，对新分支的内容进行修改吧！

#### >分支编辑步骤

1. 点击 caogao 分支的**READ.md** 文档，如下图位置点击“编辑”按钮

![Gitee 文档编辑按钮](https://images.gitee.com/uploads/images/2020/0521/154122_26b133ca_2301581.png "编辑readme.png")

---

2. 在编辑窗口中第二行添加一段内容：I am becoming better！同时**删除其他无关**的所有文字，完成后点击提交。

![Gitee 文档编辑](https://images.gitee.com/uploads/images/2020/0521/154150_5f7c0184_2301581.png "修改readme.png")

---

3. 至此，修改好的第一版 README.md 出炉了，若还需要修改可继续编辑操作。

距离胜利仅剩两步之遥啦，让我们一气呵成完成它！

---

---

### **第四步 提交 Pull Request**

Pull Requests 是 Gitee 上分支操作的一项很重要的功能。我们可以通过审核并通过 Pull Requests 从而进行不同分支的合并。

#### >提交 Pull Requests  步骤

1. 点击顶部 Pull Requests 进入配置页面，点击右上角“新建 Pull Requests” 创建请求

![Gitee 新建PR](https://images.gitee.com/uploads/images/2020/0521/154218_80f05d9a_2301581.png "新建PR.png")

---

2. 命名本次操作：PR，在文本框中填入备注信息：My first PR.

![Gitee PR 备注](https://images.gitee.com/uploads/images/2020/0521/154240_9cdfb94b_2301581.png "确认合并并备注.png")

---

3. 点击右下角创建，首个 Pull Requests 就创建完成了！

---

---

### **第五步 合并提交的 Pull Request**

恭喜你，距离胜利仅一步之遥！

作为仓库的拥有者，在合并提交的 Pull Requests 前一定要确认新分支的修改是否符合自己的期望，如果不符合可以在线反馈自己的意见。

让我们来完成这历史性的一刻吧！

#### >合并分支操作步骤

1. 进入 Pull Requests 页面，点击可合并的请求

![Gitee PR 合并确认](https://images.gitee.com/uploads/images/2020/0521/154329_9469c291_2301581.png "PR合并审查确认.png")

---

2. 可在文件选项中查看提交的分支与 master 分支有什么差别，可通过颜色对比查看

（白色：无修改；红色：删减内容；蓝色：新增内容）

![Gitee PR 文档变化对比](https://images.gitee.com/uploads/images/2020/0521/154346_1658492d_2301581.png "PR请求检查对比修改.png")

---

3. 确认无误可点击合并按钮，如有疑问可在评论中回复

![Gitee PR 确认合并界面](https://images.gitee.com/uploads/images/2020/0529/175429_7bac627e_2301581.png "PR合并审查确认或评论.png")

---

4. 点击合并中的按钮“合并分支”并再次点击“接收 Pull Requests” 

![Gitee PR 确认合并按钮](https://images.gitee.com/uploads/images/2020/0521/154655_1d904827_2301581.png "合并分支完成.png")

![Gitee PR 确认合并接受 PR](https://images.gitee.com/uploads/images/2020/0521/154721_c56901d0_2301581.png "接收PR.png")

---

5. 合并完成后显示为“已合并”，但Gitee依旧提供了强力后悔药“**回退**”功能（手快不用愁）

![Gitee 之回退功能](https://images.gitee.com/uploads/images/2020/0521/154846_c5391971_2301581.png "回退功能反悔药.png")

**恭喜您，大功告成，可以快乐的使用 Gitee 了！**

**不过瘾？学习更酷的 Gitee 使用方式，传送门：**[https://gitee.com/mvphp/gitee_advanced](https://gitee.com/mvphp/gitee_advanced)



